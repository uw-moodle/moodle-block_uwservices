<?php
/******************************************************************************
* UW Madison Services Block - Instance Settings Form
*
* Instance configuration form for UW Services block.
* 
* Author: Nick Koeppen
******************************************************************************/
class block_uwservices_edit_form extends block_edit_form {
    protected function specific_definition($mform) {
    	$config = get_config("blocks/uwservices");
    	/* Do not render form if site settings are active */
    	if($config->sitesettings){
    		return;
    	}
        $blockname = "block_uwservices";
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

        $values = array('text','icons','both');
        foreach($values as $value){
        	$options[$value] = get_string($value, $blockname);
        }
        $mform->addElement('select', 'config_display', get_string('display', $blockname), $options);
        $mform->setDefault('config_display', 'text');
    }
}