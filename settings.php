<?php
/******************************************************************************
* UW Madison Services Block - Global Settings
*
* Global (site-wide) configuration settings for UW Services block.
* 
* Author: Nick Koeppen
******************************************************************************/
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
	$blockname = 'block_uwservices';
	$plugin = 'blocks/uwservices';
	$configs = array();
	
	/*====== Settings ======*/
	$configs[] = new admin_setting_configcheckbox('sitesettings', get_string('sitesettings',$blockname), get_string('configsitesettings',$blockname), 1);
    $values = array('text','icons','both');
    $options = array();
    foreach($values as $value){
    	$options[$value] = get_string($value, $blockname);
    }	
    $configs[] = new admin_setting_configselect('display', get_string('display', $blockname), get_string('configdisplay', $blockname), 'text', $options);
	
	foreach ($configs as $config) {
        $config->plugin = $plugin;
        $settings->add($config);
    }
}