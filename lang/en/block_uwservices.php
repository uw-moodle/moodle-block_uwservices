<?php
 
$string['pluginname'] = 'UW Services';
$string['display'] = 'Display';
$string['sitesettings'] = 'Site-wide Settings';
$string['configsitesettings'] = 'Choose whether to implement site-wide settings. This overrides all local settings.';
$string['configdisplay'] = 'Choose the format of display for the links.';
$string['text'] = 'Text Only';
$string['icons'] = 'Icons Only';
$string['both'] = 'Text and Icons';

