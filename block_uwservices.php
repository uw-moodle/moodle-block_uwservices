<?php
/******************************************************************************
* UW Madison Services Block
*
* Display roster to Moodle associations and course information.
*
* Author: Nick Koeppen
******************************************************************************/
class block_uwservices extends block_list {
    function init() {
        $this->title = get_string('pluginname','block_uwservices');
    }

    function applicable_formats() {
        // Default case: the block can be used in courses and site index, but not in activities
        return array('all' => true, 'site' => true);
    }

    function has_config() {
        return true;
    }

    function get_content() {
        global $USER, $OUTPUT;

        if ($this->content !== NULL) {
            return $this->content;
        }

        // Prevents front page display
        if (!isloggedin()) {
            $this->content = NULL;
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';

        $uwservices = array();
		$uwservices[] = array(
                    'service'   => 'MyUW',
                    'relayurl'  => 'https://my.wisc.edu/portal',
                    'icon_name' => 'myuw');
        $uwservices[] = array(
                    'service'   => 'WiscMail',
                    'relayurl'  => 'https://wiscmail.wisc.edu',
                    'icon_name' => 'wiscmail');
        $domain = strstr($USER->email,'@');
        if($domain != "@wisc.edu"){
        	$uwservices[] = array(
                    'service'   => 'WiscMailPlus',
                    'relayurl'  => 'https://wiscmail.wisc.edu/mailplus/',
                    'icon_name' => 'wiscmailplus');
        }
		$uwservices[] = array(
                    'service'   => 'WiscCal',
                    'relayurl'  => 'http://webcal.services.wisc.edu',
                    'icon_name' => 'wisccal');
		$uwservices[] = array(
                    'service'   => 'Learn@UW',
                    'relayurl'  => 'http://learnuw.wisc.edu',
                    'icon_name' => 'learnuw');
		$uwservices[] = array(
                    'service'   => 'GoogleDocs',
                    'relayurl'  => 'http://docs.google.com/a/wisc.edu',
                    'icon_name' => 'googledocs');

        /* Determine origin of settings */
        $config = get_config('blocks/uwservices');
        if(!$config->sitesettings){
        	$config = $this->config;
        }
        /* Ensure default */
        if(!isset($config)){
        	$config->display = "text";
        }

        /* Display links */
        $target = 'target=\"_new\"';	//Link target
        foreach ($uwservices as $s) {
            $link = "<a ".$target.". title=\"".$s['service']."\"  href=\"".$s['relayurl']."\">&nbsp;";

            /* Add icons if not text only */
            if($config->display != "text"){
	            if($config->display == "both"){
	            	/* Place icon in list content structure (text and icons) */
	            	$url = $OUTPUT->pix_url($s['icon_name'].'_small', 'block_uwservices');
	            	$this->content->icons[] = '<img src="'.$url.'" alt="'.$s['service'].'" />';
	            }else{
	            	/* Place icon as link (icon only)*/
	                $url = $OUTPUT->pix_url($s['icon_name'], 'block_uwservices');
	                $link .= '<img src="'.$url.'" alt="'.$s['service'].'" />';
	            }
            }

            /* Add text if not only icons */
            if($config->display != "icons"){
            	$link .= $s['service'];
            }
            $link .= '</a>';

            $this->content->items[] = $link;
        }

        return $this->content;
    }
}

?>